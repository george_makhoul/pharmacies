//
//  NSBundle+Language.h
//  KCCI
//
//  Created by Ihor Embaievskyi on 1/19/18.
//  Copyright © 2018 Faifly, LCC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+ (void)setLanguage:(NSString *)language;

@end
