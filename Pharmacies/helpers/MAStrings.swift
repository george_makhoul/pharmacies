//
//  MAStrings.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/25/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import Foundation
import UIKit

extension String
{
    static let signUpButtonTitle = "Sign Up"
    static let signInButtonTitle = "Sign in"
    static let skipButtonTitle = "Skip"
    static let EmailTextFieldPlaceholder = "Email"
    static let passwordTextFieldPlaceholder = "Password"
    static let forgotPasswordButtonTitle = "Forgot password?"
    static let signUpViewTitle = "SIGN UP"
    static let firstNameTextFieldPlaceholder = "First Name"
    static let lastNameTextFieldPlaceholder = "Last Name"
    static let civilIDTextFieldPlaceholder = "Civil ID Number"
    static let confirmPasswordTextFieldPlaceholder = "Confirm Password"
    static let MobileTextFieldPlaceholder = "Mobile No"
    static let continueButtonTitle = "Continue"
    static let successfullySignedUpMessage = "Thank you for Signing up with us!"
    static let ForgotPasswordTitle = "Forgot Password"
    static let EmailEnteringExplainingText = "We just need our registered email address to send you password reset"
    static let resetPasswordBtnTitle = "Reset Password"
    static let verificationTitle = "Verification"
    static let verificationExplainingText = "Enter the verification code we just sent you on your email address"
    static let verifyBtnTitle = "Verify"
    static let ResendCodeBtnTitle = "Resend Code"
    static let ResetPasswordTitle = "Please enter your new password"
    static let NewPasswordPlaceholder = "New Password"
    static let confirmNewPasswordPlaceholder = "Confirm Password"
    static let passwordReset = "Password Reset"
    static let passwordResetSuccessfully = "Your password has been reset successfully!\n Now login with your new password"
    static let Login = "Login"
    static func timeFormatted(_ totalSeconds: Int) -> String {
             let seconds: Int = totalSeconds % 60
             let minutes: Int = (totalSeconds / 60) % 60
             return String(format: "%02d:%02d", minutes, seconds)
         }
}

