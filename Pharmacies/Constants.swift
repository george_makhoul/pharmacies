//
//  Constants.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/24/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

struct Constants
{
    static let TextFieldColor = UIColor(red: 237.0/255.0, green: 240.0/255, blue: 246.0/255.0, alpha: 1.0)
    static let ButtonColor = UIColor(red: 175.0/255.0, green: 84.0/255.0, blue: 161.0/255.0, alpha: 1.0)
    static let ButtonTextColor = UIColor(red: 63.0/255.0, green: 63.0/255.0, blue: 123.0/255.0, alpha: 1.0)
    static let underline : [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13),
        NSAttributedString.Key.foregroundColor : ButtonTextColor,
        NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
}
