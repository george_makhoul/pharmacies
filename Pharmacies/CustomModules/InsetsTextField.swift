//
//  InsetsTextField.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/19/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func addColoredPlaceHolder (_ s1: String, s2: String){
        let string1 =  NSMutableAttributedString(string: s1, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18.0),NSAttributedString.Key.foregroundColor:UIColor.gray])
        let string2 =  NSAttributedString(string: s2, attributes: [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 18.0)!,NSAttributedString.Key.foregroundColor:UIColor.purple])
        string1.append(string2)
        self.attributedPlaceholder = string1;
    }
    
    func setTFCorners( CArray: UIRectCorner, Color: CGColor, Value: Int) {
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: CArray, cornerRadii: CGSize(width: Value, height: Value)).cgPath
        self.layer.backgroundColor = Color
        self.layer.mask = rectShape
    }
}

