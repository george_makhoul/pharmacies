//
//  CView.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/20/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func setViewCorners( CArray: UIRectCorner, Color: CGColor, Value: Int) {
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: CArray, cornerRadii: CGSize(width: Value, height: Value)).cgPath
        self.layer.backgroundColor = Color
        self.layer.mask = rectShape
    }
}

