//
//  Button.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/27/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//


//

import Foundation
import UIKit

extension UIButton {
    func signingInBtnSetUp(_ image: String){
        self.layer.borderWidth = 1.0
        self.layer.borderColor = Constants.TextFieldColor.cgColor
        self.layer.cornerRadius = 10
        self.setImage(UIImage(systemName: "g.circle.fill"), for: .normal) // replace "systemName" with "name" when get the assests
    }
    
    
}
