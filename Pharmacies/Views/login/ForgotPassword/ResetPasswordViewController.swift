//
//  ResetPasswordViewController.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/27/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class ResetPasswordViewController : UIViewController {
    
    @IBOutlet weak var Table: UITableView!
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = true
        super.viewDidLoad()
        setUpTable()
    }
    
    func setUpTable() {
        self.Table.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        self.Table.register(UINib(nibName: "HeaderImageWithLockTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderImageWithLockTableViewCell")
        self.Table.register(UINib(nibName: "LabelTextTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelText")
        self.Table.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "textField")
        self.Table.allowsSelection = false
        self.Table.separatorColor = UIColor.clear
    }
    
    @objc func goBack () {
        self.navigationController?.popViewController(animated: true)
    }
    @objc public func resetPasswordSuccess() {
           let vc  = (storyboard?.instantiateViewController(identifier: "ResetPasswordSuc"))! as ResetPasswordSuccessfullyViewController
           self.navigationController?.pushViewController(vc, animated: true)
       }
    
    
}

extension ResetPasswordViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 340
        }
        if indexPath.row == 2 {
            return 40
        }
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let HeaderCell = tableView.dequeueReusableCell(withIdentifier: "HeaderImageWithLockTableViewCell") as! HeaderImageWithLockTableViewCell
        let titleCell = tableView.dequeueReusableCell(withIdentifier: "LabelText") as! LabelTextTableViewCell
        let TextFiledCell = tableView.dequeueReusableCell(withIdentifier: "textField") as! TextFieldTableViewCell
        let buttonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as! ButtonCell
        if indexPath.row == 0 {
            HeaderCell.backBtn.isHidden = false
            HeaderCell.backBtn.addTarget(self, action: #selector(goBack), for: .touchUpInside)
            HeaderCell.setUpCellImg("lock.fill")
            return HeaderCell
        }
        if indexPath.row == 1 {
            titleCell.SetupUI(String.resetPasswordBtnTitle.localized, size: 22.0, textColor: Constants.ButtonTextColor)
            return titleCell
        }
        if indexPath.row == 2 {
            titleCell.SetupUI(String.ResetPasswordTitle.localized, size: 16.0, textColor: UIColor.lightGray)
            return titleCell
        }
        if indexPath.row == 3 {
            TextFiledCell.textField.placeholder = String.NewPasswordPlaceholder.localized
            TextFiledCell.textField.backgroundColor = Constants.TextFieldColor
            return TextFiledCell
        }
        if indexPath.row == 4 {
            TextFiledCell.textField.placeholder = String.confirmNewPasswordPlaceholder.localized
            TextFiledCell.textField.backgroundColor = Constants.TextFieldColor
            return TextFiledCell
        }
        if indexPath.row == 5 {
            buttonCell.Setup(String.resetPasswordBtnTitle.localized, corner: 25, BackgroundColor: Constants.ButtonColor, titleColor: UIColor.white, leading: 40, trailing: 40)
            buttonCell.Btn.addTarget(self, action: #selector(resetPasswordSuccess), for: .touchUpInside)
            return buttonCell
        }
        return HeaderCell
    }
    
    
    
    
}
