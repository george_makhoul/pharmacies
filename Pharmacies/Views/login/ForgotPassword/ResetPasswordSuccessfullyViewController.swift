//
//  ResetPasswordSuccessfullyViewController.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/27/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class ResetPasswordSuccessfullyViewController: UIViewController {
    
    @IBOutlet weak var Table: UITableView!
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = true
        super.viewDidLoad()
        setUpTable()
    }
    func setUpTable() {
        self.Table.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        self.Table.register(UINib(nibName: "HeaderImageWithLockTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderImageWithLockTableViewCell")
        self.Table.register(UINib(nibName: "LabelTextTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelText")
        self.Table.allowsSelection = false
        self.Table.separatorColor = UIColor.clear
    }
    @objc func goBack () {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func gotoLogin () {
        let vc  = (storyboard?.instantiateViewController(identifier: "login"))! as LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension ResetPasswordSuccessfullyViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 340
        }
        if indexPath.row == 2 {
            return 130
        }
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let HeaderCell = tableView.dequeueReusableCell(withIdentifier: "HeaderImageWithLockTableViewCell") as! HeaderImageWithLockTableViewCell
        let titleCell = tableView.dequeueReusableCell(withIdentifier: "LabelText") as! LabelTextTableViewCell
        let buttonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as! ButtonCell
        
        if indexPath.row == 0 {
            HeaderCell.setUpCellImg("lock.fill")
            HeaderCell.backBtn.isHidden = false
            HeaderCell.backBtn.addTarget(self, action: #selector(goBack), for: .touchUpInside)
            HeaderCell.setUpCellImg("lock.fill")
            return HeaderCell
        }
        if indexPath.row == 1 {
            titleCell.SetupUI(String.passwordReset.localized, size: 22.0, textColor: Constants.ButtonTextColor)
            return titleCell
        }
        if indexPath.row == 2 {
            titleCell.SetupUI(String.passwordResetSuccessfully.localized, size: 18.0, textColor: UIColor.lightGray)
            return titleCell
        }
        if indexPath.row == 3 {
            buttonCell.Setup(String.Login, corner: 25, BackgroundColor: Constants.ButtonColor, titleColor: UIColor.white, leading: 40.0, trailing: 40.0)
            buttonCell.Btn.addTarget(self, action: #selector(gotoLogin), for: .touchUpInside)
            return buttonCell
        }
        
        return HeaderCell
    }
    
    
    
    
}
