//
//  ForgotPasswordViewController.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/25/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class ForgotPasswordViewController : UIViewController {
    
    @IBOutlet weak var Table: UITableView!
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = true
        setUpTableView()
        super.viewDidLoad()
    }
    func setUpTableView() {
        self.Table.register(UINib(nibName: "HeaderImageWithLockTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderImageWithLockTableViewCell")
        self.Table.register(UINib(nibName: "LabelTextTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelText")
        self.Table.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "textField")
        self.Table.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        self.Table.register(UINib(nibName: "DontHaveAccountCell", bundle: nil), forCellReuseIdentifier: "DontHaveAcc")
        self.Table.allowsSelection = false
        self.Table.separatorColor = UIColor.clear
    }
    @objc public func SignUpAction(_ sender: UIButton) {
        let vc  = (storyboard?.instantiateViewController(identifier: "signUp"))! as signUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc public func resetPassword() {
        let vc  = (storyboard?.instantiateViewController(identifier: "verification"))! as VerificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc public func goback() {
        self.navigationController?.popViewController(animated: true)
    }
}
extension ForgotPasswordViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 340
        }
        if indexPath.row == 2 {
            return 100
        }
        if indexPath.row == 6 {
            return 100
        }
        
        return 70
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let HeaderCell = tableView.dequeueReusableCell(withIdentifier: "HeaderImageWithLockTableViewCell") as! HeaderImageWithLockTableViewCell
        let titleCell = tableView.dequeueReusableCell(withIdentifier: "LabelText") as! LabelTextTableViewCell
        let TextFiledCell = tableView.dequeueReusableCell(withIdentifier: "textField") as! TextFieldTableViewCell
        let buttonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as! ButtonCell
        let DonthaveAcc = tableView.dequeueReusableCell(withIdentifier: "DontHaveAcc") as! DontHaveAccountCell
        if indexPath.row == 0 {
            HeaderCell.backBtn.isHidden = false
            HeaderCell.setUpCellImg("lock.fill")
            HeaderCell.backBtn.addTarget(self, action: #selector(goback), for: .touchUpInside)
            return HeaderCell
        }
        if indexPath.row == 1 {
            titleCell.SetupUI(String.forgotPasswordButtonTitle, size: 22.0, textColor: Constants.ButtonTextColor)
            return titleCell
        }
        if indexPath.row == 2 {
            titleCell.SetupUI(String.EmailEnteringExplainingText, size: 18.0, textColor: UIColor.lightGray)
            return titleCell
            
        }
        if indexPath.row == 3 {
            titleCell.labelText.text = ""
            return titleCell
        }
        if indexPath.row == 4 {
            TextFiledCell.textField.placeholder = String.EmailTextFieldPlaceholder.localized
            TextFiledCell.textField.backgroundColor = Constants.TextFieldColor
            return TextFiledCell
        }
        
        if indexPath.row == 5 {
            buttonCell.Setup(String.resetPasswordBtnTitle.localized, corner: 25, BackgroundColor: Constants.ButtonColor, titleColor: UIColor.white, leading: 40.0, trailing: 40.0)
            buttonCell.Btn.addTarget(self, action: #selector(resetPassword), for: .touchUpInside)
            return buttonCell
        }
        if indexPath.row == 6 {
            titleCell.labelText.text = ""
            return titleCell
        }
        if indexPath.row == 7 {
            DonthaveAcc.signUpBtn.setTitle(String.signUpButtonTitle.localized, for: .normal)
            DonthaveAcc.signUpBtn.addTarget(self,action: #selector(SignUpAction(_:)), for: .touchUpInside)
            titleCell.labelText.text = ""
            return DonthaveAcc
        }
        return HeaderCell
        
    }
    
    
    
    
}
