//
//  VerificationViewController.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/26/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class VerificationViewController: UIViewController {
    
    @IBOutlet weak var Table: UITableView!
    
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = true
        
        
        setUpTableView()
        super.viewDidLoad()
        
    }
    
    func setUpTableView() {
        self.Table.register(UINib(nibName: "HeaderImageWithLockTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderImageWithLockTableViewCell")
        self.Table.register(UINib(nibName: "LabelTextTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelText")
        self.Table.register(UINib(nibName: "VerificationTableViewCell", bundle: nil), forCellReuseIdentifier: "verificationCell")
        self.Table.register(UINib(nibName: "TimerTableViewCell", bundle: nil), forCellReuseIdentifier: "TimerCell")
        self.Table.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        self.Table.allowsSelection = false
        self.Table.separatorColor = UIColor.clear
    }
    
    @objc public func goBack () {
        self.navigationController?.popViewController(animated: true)
    }
    @objc public func goToResetPassword() {
        let vc  = (storyboard?.instantiateViewController(identifier: "ResetPassword"))! as ResetPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
extension VerificationViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 340
        }
        if indexPath.row == 1 {
            return 50
        }
        if indexPath.row == 4 || indexPath.row == 5{
            return 40
        }
        
        return 70
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let HeaderCell = tableView.dequeueReusableCell(withIdentifier: "HeaderImageWithLockTableViewCell") as! HeaderImageWithLockTableViewCell
        let titleCell = tableView.dequeueReusableCell(withIdentifier: "LabelText") as! LabelTextTableViewCell
        let verificationCell = tableView.dequeueReusableCell(withIdentifier: "verificationCell") as! VerificationTableViewCell
        let timerCell = tableView.dequeueReusableCell(withIdentifier: "TimerCell") as! TimerTableViewCell
        let buttonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as! ButtonCell
        
        if indexPath.row == 0 {
            HeaderCell.backBtn.isHidden = false
            HeaderCell.setUpCellImg("lock.fill")
            HeaderCell.backBtn.addTarget(self, action: #selector(goBack), for: .touchUpInside)
            return HeaderCell
        }
        if indexPath.row == 1 {
            titleCell.SetupUI(String.verificationTitle.localized, size: 22.0, textColor: Constants.ButtonTextColor)
            return titleCell
        }
        if indexPath.row == 2 {
            titleCell.SetupUI(String.verificationExplainingText, size: 16.0, textColor: UIColor.lightGray)
            return titleCell
        }
        
        if indexPath.row == 3 {
            return verificationCell
        }
        if indexPath.row == 4 {
            return timerCell
        }
        if indexPath.row == 5 {
            titleCell.SetupUI("", size: 0.0, textColor: UIColor.clear)
            return titleCell
        }
        if indexPath.row == 6 {
            buttonCell.Setup(String.verifyBtnTitle.localized, corner: 25, BackgroundColor: Constants.ButtonColor, titleColor: UIColor.white, leading: 40.0, trailing: 40.0)
            buttonCell.Btn.addTarget(self, action: #selector(goToResetPassword), for: .touchUpInside)
            return buttonCell
        }
        if indexPath.row == 7 {
            titleCell.SetupUI("", size: 0.0, textColor: UIColor.clear)
            return titleCell
        }
        if indexPath.row == 8 {
            let attributeString = NSMutableAttributedString(string: String.ResendCodeBtnTitle.localized,
                                                            attributes: Constants.underline)
            buttonCell.Btn.setAttributedTitle(attributeString, for: .normal)
            return buttonCell
        }
        
        
        return HeaderCell
    }
    
    
    
    
}
