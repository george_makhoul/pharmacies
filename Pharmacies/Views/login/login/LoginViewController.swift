//
//  LoginViewController.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/17/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var TableView: UITableView!
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = true
        setUpTableView()
        super.viewDidLoad()
    }
    
    func setUpTableView() {
        self.TableView.register(UINib(nibName: "HeaderImageCell", bundle: nil), forCellReuseIdentifier: "headerCell")
        self.TableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "textField")
        self.TableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        self.TableView.register(UINib(nibName: "signingInCellTableViewCell", bundle: nil), forCellReuseIdentifier: "signingInCell")
        self.TableView.register(UINib(nibName: "DontHaveAccountCell", bundle: nil), forCellReuseIdentifier: "DontHaveAcc")
        self.TableView.register(UINib(nibName: "LabelTextTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelText")
        self.TableView.allowsSelection = false
        self.TableView.separatorColor = UIColor.clear
    }
    @objc public func BtnClick(_ sender: UIButton) {
        print("Hello")
    }
    
    @objc public func SignUpAction(_ sender: UIButton) {
        let vc  = (storyboard?.instantiateViewController(identifier: "signUp"))! as signUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
       }
    
    @objc public func ForgotPassword(_ sender: UIButton) {
        let vc = (storyboard?.instantiateViewController(identifier:"forgotPassword"))! as ForgotPasswordViewController
     self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension LoginViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 300
        }
        if indexPath.row == 1 {
            return 80
        }
        return 70
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let HeaderCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! HeaderImageCell
        let TextFiledCell = tableView.dequeueReusableCell(withIdentifier: "textField") as! TextFieldTableViewCell
        let buttonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as! ButtonCell
        let signingInCell = tableView.dequeueReusableCell(withIdentifier: "signingInCell") as! signingInCellTableViewCell
        let DonthaveAcc = tableView.dequeueReusableCell(withIdentifier: "DontHaveAcc") as! DontHaveAccountCell
        let titleCell = tableView.dequeueReusableCell(withIdentifier: "LabelText") as! LabelTextTableViewCell
        if indexPath.row == 0 {
            HeaderCell.headerImg.image = UIImage(named: "Header")
            return HeaderCell
        }
        if indexPath.row == 1 {
            HeaderCell.headerImg.image = nil
            return HeaderCell
        }
        if indexPath.row == 2 {
            TextFiledCell.textField.placeholder = String.EmailTextFieldPlaceholder.localized
            TextFiledCell.textField.backgroundColor = Constants.TextFieldColor
            return TextFiledCell
        }
        if indexPath.row == 3 {
            TextFiledCell.textField.placeholder = String.passwordTextFieldPlaceholder.localized
            TextFiledCell.textField.backgroundColor = Constants.TextFieldColor
            return TextFiledCell
        }
        if indexPath.row == 4 {
            buttonCell.Setup(String.signInButtonTitle.localized, corner: 25, BackgroundColor: Constants.ButtonColor, titleColor: UIColor.white, leading: 20.0, trailing: 20.0)
            buttonCell.Btn.addTarget(self, action: #selector(BtnClick(_:)), for: .touchUpInside)
            return buttonCell
        }
        if indexPath.row == 5 {
            buttonCell.Setup(String.forgotPasswordButtonTitle.localized, corner: 0, BackgroundColor: UIColor.clear, titleColor: UIColor(red: 63.0/255.0, green: 63.0/255.0, blue: 123.0/255.0, alpha: 1.0), leading: 0.0, trailing: 0.0)
            buttonCell.Btn.titleLabel?.font = .systemFont(ofSize: 15)
            buttonCell.Btn.addTarget(self, action: #selector(ForgotPassword(_:)), for: .touchUpInside)
            return buttonCell
        }
        if indexPath.row == 6 {
            titleCell.SetupUI("or sign in with", size: 16.0, textColor: UIColor.lightGray)
            return titleCell
        }
        if indexPath.row == 7 {
            
            return signingInCell
        }
        if indexPath.row == 8 {
            DonthaveAcc.signUpBtn.setTitle(String.signUpButtonTitle.localized, for: .normal)
            DonthaveAcc.signUpBtn.addTarget(self,action: #selector(SignUpAction(_:)), for: .touchUpInside)
            return DonthaveAcc
        }
        if indexPath.row == 9 {
            buttonCell.Setup(String.skipButtonTitle.localized, corner: 0, BackgroundColor: UIColor.clear, titleColor: Constants.ButtonColor, leading: 0.0, trailing: 0.0)
            buttonCell.Btn.titleLabel?.font = .systemFont(ofSize: 15)
            return buttonCell
        }
        
        
        return HeaderCell
        
    }
    
    
}
