//
//  ViewController.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/17/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class signInViewController: UIViewController {
    
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    func setupUI() {
        self.signInBtn.layer.cornerRadius = 22.5
        self.signUpBtn.layer.cornerRadius = 22.5
        self.signInBtn.setTitle(String.signInButtonTitle.localized, for: .normal)
        self.signUpBtn.setTitle(String.signUpButtonTitle.localized, for: .normal)
        self.skipBtn.setTitle(String.skipButtonTitle.localized, for: .normal)
    }
    
    @IBAction func signInAction(_ sender: UIButton) {
        let vc  = (storyboard?.instantiateViewController(identifier: "login"))! as LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    
}
    @IBAction func signUpAction(_ sender: UIButton) {
        let vc  = (storyboard?.instantiateViewController(identifier: "signUp"))! as signUpViewController
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
