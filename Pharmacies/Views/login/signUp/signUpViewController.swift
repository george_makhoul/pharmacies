//
//  signUpViewController.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/19/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class signUpViewController: UIViewController {
    @IBOutlet weak var ViewTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUpTableView()
        setUpUI()
    }
    
    func setUpTableView() {
        self.tableView.register(UINib(nibName: "signUpTableViewCell", bundle: nil), forCellReuseIdentifier: "signUpCell")
        self.tableView.register(UINib(nibName: "CountryPickerCell", bundle: nil), forCellReuseIdentifier: "PickerCell")
        self.tableView.register(UINib(nibName: "TermsAndConditionCell", bundle: nil), forCellReuseIdentifier: "Terms")
        self.tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        self.tableView.allowsSelection = false
        self.tableView.separatorColor = UIColor.clear
    }
    
    func setUpUI(){
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 63.0/255.0, green: 63.0/255.0, blue: 123.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.isHidden = true
        self.ViewTitle.text = String.signUpViewTitle.localized
        
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    @objc public func SignUp(_ sender: UIButton) {
        let vc  = (storyboard?.instantiateViewController(identifier: "success"))! as SignedUpSuccessfully
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
extension signUpViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "signUpCell") as! signUpTableViewCell
        let pickerCell = tableView.dequeueReusableCell(withIdentifier: "PickerCell") as! CountryPickerCell
        let termsCell = tableView.dequeueReusableCell(withIdentifier: "Terms") as! TermsAndConditionCell
        let buttonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as! ButtonCell
        cell.contentView.backgroundColor = .clear
        pickerCell.contentView.backgroundColor = .clear
        
        if indexPath.row == 0 {
            cell.CellImg.image = UIImage(systemName: "person.fill")
            cell.CellTextField.addColoredPlaceHolder(String.firstNameTextFieldPlaceholder.localized, s2: "*")
        }
        if indexPath.row == 1 {
            cell.CellImg.image = UIImage(systemName: "person.fill")
            cell.CellTextField.addColoredPlaceHolder(String.lastNameTextFieldPlaceholder.localized, s2: "*")
        }
        if indexPath.row == 2 {
            cell.CellImg.image = UIImage(systemName: "creditcard.fill")
            cell.CellTextField.addColoredPlaceHolder(String.civilIDTextFieldPlaceholder.localized, s2: "*")
        }
        if indexPath.row == 3 {
            cell.CellImg.image = UIImage(systemName: "envelope.fill")
            cell.CellTextField.addColoredPlaceHolder(String.EmailTextFieldPlaceholder.localized, s2: "*")
        }
        if indexPath.row == 4 {
            return pickerCell
        }
        if indexPath.row == 5 {
            cell.CellImg.image = UIImage(systemName: "lock.fill")
            cell.CellTextField.addColoredPlaceHolder(String.passwordTextFieldPlaceholder.localized, s2: "*")
        }
        if indexPath.row == 6 {
            cell.CellImg.image = UIImage(systemName: "lock.fill")
            cell.CellTextField.addColoredPlaceHolder(String.confirmPasswordTextFieldPlaceholder.localized, s2: "*")
        }
        if indexPath.row == 7 {
            return termsCell
        }
        if indexPath.row == 8 {
            buttonCell.Setup(String.signUpButtonTitle.localized, corner: 25, BackgroundColor: Constants.ButtonColor, titleColor: UIColor.white, leading: 0.0, trailing: 0.0)
            buttonCell.Btn.addTarget(self, action: #selector(SignUp(_:)), for: .touchUpInside)
            return buttonCell
        }
        return cell
    }
    
    
    
    
}
