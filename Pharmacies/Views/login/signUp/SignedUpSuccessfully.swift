//
//  SignedUpSuccessfully.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/25/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class SignedUpSuccessfully: UIViewController {

    @IBOutlet weak var signedUpSuccessfullyMsg: UILabel!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var image: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        // Do any additional setup after loading the view.
    }
    func setUpUI() {
        self.image.layer.cornerRadius = self.image.frame.width / 2
        self.continueBtn.backgroundColor = Constants.ButtonColor
        self.continueBtn.setTitleColor(UIColor.white, for: .normal)
        self.continueBtn.layer.cornerRadius = 25
        self.continueBtn.setTitle(String.continueButtonTitle.localized, for: .normal)
        self.signedUpSuccessfullyMsg.text = String.successfullySignedUpMessage.localized
    }


}
