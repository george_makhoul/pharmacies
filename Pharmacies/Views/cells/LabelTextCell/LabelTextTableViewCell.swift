//
//  LabelTextTableViewCell.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/25/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class LabelTextTableViewCell: UITableViewCell {

    @IBOutlet weak var labelText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func SetupUI(_ Text: String, size:CGFloat, textColor: UIColor) {
        self.labelText.text = Text.localized
        self.labelText.font = .boldSystemFont(ofSize: size)
        self.labelText.textColor = textColor
    }
    
}
