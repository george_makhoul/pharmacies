//
//  signingInCellTableViewCell.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/24/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class signingInCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var trailingConstraints: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraints: NSLayoutConstraint!
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var appleBtn: UIButton!
    @IBOutlet weak var googleBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        ButtonsSetUp()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
    func ButtonsSetUp() {
        self.googleBtn.signingInBtnSetUp("g.circle.fill")
        self.facebookBtn.signingInBtnSetUp("f.circle.fill")
        self.appleBtn.signingInBtnSetUp("g.circle.fill")
        self.leadingConstraints.constant = 80
        self.trailingConstraints.constant = 80
    }
    
}

