//
//  DontHaveAccountCell.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/24/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class DontHaveAccountCell: UITableViewCell {
    
    @IBOutlet weak var signUpBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
    }
}
