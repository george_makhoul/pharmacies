//
//  HeaderImageWithLockTableViewCell.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/25/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class HeaderImageWithLockTableViewCell: UITableViewCell {
    
    @IBOutlet weak var circleImage: UIImageView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var headerImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.circleView.layer.cornerRadius = 40
        self.headerImage.image = UIImage(named: "Header")
        self.backBtn.isHidden = true
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setUpCellImg (_ imageName: String) {
        self.circleImage.image = UIImage(systemName: imageName)
    }
    
}
