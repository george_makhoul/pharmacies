//
//  ButtonCell.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/20/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class ButtonCell: UITableViewCell {
    
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var Btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func Setup (_ title: String, corner: CGFloat, BackgroundColor: UIColor, titleColor: UIColor, leading: CGFloat, trailing: CGFloat) {
        self.Btn.setTitle(title, for: .normal)
        self.Btn.layer.cornerRadius = corner
        self.Btn.backgroundColor = BackgroundColor
        self.Btn.setTitleColor(titleColor, for: .normal)
        self.leadingConstraint.constant = leading
        self.trailingConstraint.constant = trailing

    }

    func target(_ target: Any?, _ sel: Selector) {
        Btn.addTarget(target, action: sel, for: .touchUpInside)
    }
    
}
