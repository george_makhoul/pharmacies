//
//  TextFieldTableViewCell.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/24/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    override func awakeFromNib() {
        self.textField.layer.cornerRadius = 25
        self.textField.clipsToBounds = true
        textField.setLeftPaddingPoints(30)
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
