//
//  TimerTableViewCell.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/26/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class TimerTableViewCell: UITableViewCell {
    @IBOutlet weak var timerLbl: UILabel!
    var totalTime = 360
    var timer: Timer?
    override func awakeFromNib() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        super.awakeFromNib()
        // Initialization code
    }
    @objc func updateTimer() {
        
        self.timerLbl.text = String.timeFormatted(self.totalTime)
        if totalTime != 0 {
            totalTime -= 1
        } else {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
