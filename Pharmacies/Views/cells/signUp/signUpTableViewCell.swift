//
//  signUpTableViewCell.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/19/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class signUpTableViewCell: UITableViewCell {
    @IBOutlet weak var CellTextField: UITextField!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var CellImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellView.layer.cornerRadius = 25
        self.CellImg.tintColor = .purple
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
