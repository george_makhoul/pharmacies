//
//  VerificationTableViewCell.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/26/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//

import UIKit

class VerificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var FrthCodeNumber: UITextField!
    @IBOutlet weak var thrdCodeNumber: UITextField!
    @IBOutlet weak var secCodeNumber: UITextField!
    @IBOutlet weak var firstCodeNumber: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupTextField(firstCodeNumber)
        setupTextField(secCodeNumber)
        setupTextField(thrdCodeNumber)
        setupTextField(FrthCodeNumber)
        
    }
    
    func setupTextField(_ textField: UITextField){
        textField.delegate = self
        textField.layer.cornerRadius = 25
        textField.clipsToBounds = true
        textField.backgroundColor = Constants.TextFieldColor
        textField.textAlignment = .center
        textField.keyboardType = .numberPad
        textField.textColor = Constants.ButtonTextColor
        textField.tintColor = Constants.ButtonColor
        textField.font = UIFont(name: "HelveticaNeue-Bold", size: 20.0)
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension VerificationTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !(string == "") {
            textField.text = string
            if textField == firstCodeNumber {
                secCodeNumber.becomeFirstResponder()
            }
            else if textField == secCodeNumber {
                thrdCodeNumber.becomeFirstResponder()
            }
            else if textField == thrdCodeNumber {
                FrthCodeNumber.becomeFirstResponder()
            }
            else {
                textField.resignFirstResponder()
            }
            return false
        }
        if (string == "") {
            textField.text = string
            if textField == firstCodeNumber {
                
            }
            else if textField == secCodeNumber {
                firstCodeNumber.becomeFirstResponder()
            }
            else if textField == thrdCodeNumber {
                secCodeNumber.becomeFirstResponder()
            }
            else {
                thrdCodeNumber.becomeFirstResponder()
            }
            return false
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField.text?.count ?? 0) > 0 {
            
        }
        return true
    }
}
