//
//  CountryPickerCell.swift
//  Pharmacies
//
//  Created by George Makhoul on 1/20/21.
//  Copyright © 2021 George Makhoul. All rights reserved.
//
import Foundation
import UIKit

class CountryPickerCell: UITableViewCell {
    
    @IBOutlet weak var TextField: UITextField!
    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var pickerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pickerView.setViewCorners(CArray: [.topLeft,.bottomLeft], Color: UIColor.white.cgColor, Value: 25)
        CellView.layer.cornerRadius = 25
        TextField.addColoredPlaceHolder(String.MobileTextFieldPlaceholder.localized, s2: "*")
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
